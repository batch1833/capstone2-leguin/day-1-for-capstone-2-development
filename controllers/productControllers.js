const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

//Create Products
module.exports.addProduct = (reqBody) =>{
	let newProduct = new Product({
		productName: reqBody.productName,
		description: reqBody.description,
		stocks: reqBody.stocks,
		price: reqBody.price

	})
	return newProduct.save().then((product, error)=>{

		if(error){
			return "Failed"
			//return false;
		}

		else{
			return "Product Created Successful"
			//return true;
		}

	})
}

//Retrieve all products

module.exports.getAllActiveProducts = () => {

	return Product.find({isActive: true}).then(result => result);
}


// Retrieving a specific product

module.exports.getProduct = (productId) =>{
	return Product.findById(productId).then(result => result);
}


// Update a product info

module.exports.updateProduct = (productId, reqBody) =>{
	// Specify the fields / properties to be updated
	let updatedProduct = {
		productName: reqBody.productName,
		description: reqBody.description,
		price: reqBody.price,
		stocks: reqBody.stocks,
		isActive: reqBody.isActive
	}


	return Product.findByIdAndUpdate(productId, updatedProduct).then((productUpdate, error)=>{
		if(error){
			return false;
		}
		else{
			return "Update Successful";
			//return true;
		}
	})
}


//ARCHIVE / CRUD
module.exports.archiveProduct = (productId) =>{
    let updateActiveField = {
        isActive : false
    }
    return Product.findByIdAndUpdate(productId, updateActiveField).then((isActive, error) =>{
        
        if(error){
        	return "Failure to delete"
        	//return false;
        }
        

        else{
        	return "Temporarily deleted One"
            //return true
        }
    })
}

